import React, {Component} from 'react';
import {Grid, Cell, List, ListItem, ListItemContent} from 'react-mdl';



class Contact extends Component {
    render (){
        return(
            <div className="contact-body">
                <Grid className="contact-grid">
                    <Cell col={6}>
                        <h2>Arkadiusz Krezel</h2>
                        <img
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABg1BMVEX/xhv////UiwchJzNHTlsrM0Q2PEdx4u84xtkqLzU/REwnKTX/wgD/yRw6QEv/xAAfJTE6QVDwshXZjgAsOEk7SF0uNDoRITSxkT6KZS+mhjKudhsXKDeIXiMZKkXRhgTRgQA3xdnvsRX/78j/9uH/zRf0uBY1RF7brSf/1m916/i4fRu7mDsfEiD/yzb/+OcgGif/5qsAHTX/3Ib/zkr/4ZogGCX/569EdoH//PQvjJwsMT0lIi//0l4+Z3IgCRz/8MzHoDYKJEb/4p5XpLAmUl4rdIIkMkm9mC8AABRozNk0rL7/z0//6bswRE/GhBNcsr5zUyfYlCJKRkSLeEoVITdfV1EAABPw1bNyYjrisW0AHEaXezVwXkuGaEJ0XD+ody7eplFQQC/ryp7WpiR+cE5cRSrfqFlEOS9wcHDDw78CEiV+gYeho6ZiZGm5ubjf3diXl5cXFhk0MzJ3cmuKcjfw06fpwYkACSyunYa/tJiEVAOkgR1YTUFMi5dYWFeOaz27NHVoAAAQkUlEQVR4nNXd+VcT1x4A8JkERoFMZngJRl9lUQiyNCEQCIQgAQSi7EpBfWq1FvtUqGArr32Ly5/+Zkky293vnWT6Pae/0HNgPn7v+p07M5IcepSnVnan54rjO7P5vCpJkprPz+6MF+emd1emyuH/eSnMXz61Ml2clbSEZoXkDvtHCU2aLU6vTIV5EWEJp3aLOxZNwoUF3SnuhsUMQzg1Py6R2HxOaXw+DKVoYXm4mE9Q6hxlIl8cFt01hQrLu+O0uQPkcnxX5DWJFA5z8xzksLjLEiUcKYrhNZHFEUFXJkY4P5sQx6sjE7PzQq5NgLA8JzB7HqQ2J2Bw5RaOjIfks43j3I2VUzhyV3jz9BkTdzmNXMKRuyHmT5SRQzgVdv5cRo7+yCwsF1vks4xakXmpwyqcbkH79BqnWyocybfWZxnzbN2RSVhMtNxnRqLYIuGw1PoE2qFJDOtVeuF4exJoR2I8dOFK2xJohyathCuca2cC7UjMhSgsz7Y3gXZos1RzI41wpcVzICw0jaalUggj0EIbQdNSyYV3o5FAO7S7woXlNqxiUKHlSTsjoXAkIl3QCU0jXMSRCYej5jNDI1vgEAmnozPGuCNBtN0gEd6LJpCQSCCM0CzhD5JZAy+MMJCIiBVGGkhCxAkF9kHVE6J+a+Ien1DMKKqq2aw0OnZysrq6et+M1ZMxKZvNCnHihhu0cJ4fqGbV0dXnbzrumNHX11ezY2Bg4PMP98eM/8tPRN/fQApXuIGqOnb51YR1NCKpNCNdG+i+POFHJpBbDZRwhHMlo2ZHn3fccXB+oaUcSD8f4zUiF3AIYZnXN/amz8cLCo3oHHh5wmtELMMRQr7dhOHzpw8iNBP5YizL88e0PIuQaz+oSpdAH1hoGi8lnjQi9otQ4RwPMLvaAfbBhEZbTa/ypFGDzvww4TDHMKpKP9yB+OBCRRl4ypNG6IAKEZY5MqiOHsMSiBQq6cNRDqIGGW0gQo5RJjuG8CGFxgR5wt5SYaMNWFjkAK7CWyhOaLTU+xxE8I0boJBjLYMDYoTKGgcR3BWBQua/gQfihFxZBGMAPxtnbqPqGA6IFSpr7H1RA92ZAgjZJwp1FDnIkAmV2gnziJoAlN8AQvZRRv2KBRIIFeVvzESNRMg+jmZ/wKeQRJj+hZkIGE8DQvZxVMWOMqQ5rL1iJgbH04AwzwqUJAIfmVBJ7zMTA/O+XzgdbhslFb5M3WYkBs7d+ITs61GCiYJcqNQmB5mJZaSQY5h5Q5RCQqHyOcVK9A82XuEU+zBDmEJSYe07ZmJiCiFkX82QppBUaCQxxkj0rWw8whH2FfcoYQqJhbXJGCsxMQIVspdmspeEKSQWGsMpK9FbtHELOVJINhfSCBUlFmMlepLoFrKnkGw5QydMv0qxEj1JdAk5Ukg429MJzWbKSHQn0SVkH0ilLHEjpWilaUvIRHQPp45wimPXRDoZ0gknY+zEKYCQowSsfk/cSGmE/SlmoqtA7Ag5KqQU3ZBG+LIuZCIGhfM8wmNiIIVQOWwIGYjafEA4yw6kmA2phOlYjJ046xfyzPbkSzY6YW2fg9icMBpCjio31VBKlcPvUuzE5iaqIeS6WUi+oqETvnIJ6Yle4S6fkHwoZRfGBulqN9quR8ixnqGbDjmElFlsrGsk/kYanrDfK6QluoVcjbR1QjpivZlK/I2UUNibtHCVejAJqYj1ZirxN1Kk0GRVelwRd0cP0goQ0hEdIecxbqCw18hWDyDiwABCQUKaEdU+CC7xTvdBYVIB0pBC3fqvx+sECimyaE/6lpDjXoUJzF5vCJPgvDVjyQh9SdfBzGY+G8IbKT5iviFkrwNbwOv100EKTFUolHK5aq6nsre3sbyxsbGXjhdyuVypUIBg9biNTKe/AYmkDdWqDUucGydJfVNfsgVpBqxa6tlb/vJl/WBr64oRExMTV624dXV7e3Fxc3OjoptUI7N+ZT2NhwAhcRatLZTEOVdknwOASwUjZcryggmbMOOKEwbOGyZ1eSNeNaF6kPiSo6Fa84UpZPcZG6d6F1QaTTJXXVKWvxxsWQm7EoyAsB63theNjJZyTWa9N6b3AULiLNpCjhKUs62wfIXq3sL6FoSGETady5XqktUZ68JXoCQS9kWzICXxLdnU665RprRwBWUjEdrMzYKTRGNABQnJiObCTeLc/NaFVgvdw/LIhFev7unNnggTEhHNGVHiq9C4haV1YcLFAl5IRJy1hFxzhS1MmsLqFgGQTLidIxCSEDVTyFWDcgsrJEAy4VWnIyKEBERjzpf4lt11oULcDQmFVkfECvFEY/EtyfdECTdIhBMkvqtXN8iEWKJ2zxDylWjohBMHCxub2wKFOKKxqpH4NhZ0womFqrEErS4LFOKIeUPIA3SPNPh+OLFetfaHuUWsME0sxBFliedUflPYay3asI20VN8B53Cjza0S0VhKQNTKEtdk4Znxqzjgeq4uLOGSeItoPiQhJkYkvhqNW5g7wDTS5aW6cGkDI9wu0QhRRG1Y4tr+eldtXzAdsdKs0yxhmukmyaqNiKjtSlzTYVNoXvzSMlq4VbVWPlZHxMwYG+b+qYdcCCdq9yS+Optn91Qg6IY9SVNYQHdE10BDKIQStaIgIUFHnFgwu2ElSdARF0uuHTCZEEY0hJwFfbcQ3UwnrBlFSVrNtIBvpHE6IYSojUs7PEBHqOD2TxMHViPttYXI+WK76q7TkAohxB2J64SCI7SSWFiAJXFi4sCuFXckFbvQhCAuLzmNlEIIJs5KnPXuprACT+LEla0vlardSM2avyXM6bAluL39jdMLgcS8MGEvqFRj1kq31hcq1UK9kmrdP6wXtgu5+PIiQGmtSfUKgxBEzHM9X+y5KWMlsbRsVtvqVeCtg/WFjVy1tNQsFSctYaVR+NWXCrnc3vLi4rZrBWAPM40UQm7PUDVUPuGJcwzDLpnurR+sr39ZWN6rFKq50pKjs4HWPeCK5yaFWf0vxdMbm5ubi4uLFXexVPEdOWkL0XmYyy57L5Xsey4eWr0PdnQ07nJ7jBZJN+/gFEoF3V3wNoPGFwbRfZamogdUzagkm3cX7ahA7iRa3GYbVWrgkjcNkVecXW28WqCv45cjsE7pbv4ruE8qVCpx4N21zFslXW+i9EA/UeUcS03i7W/HZry4EUv1Hx3FfbjeDm/4z2JUzGS6nHpm5mFq/8bnQzO+7dMDfUTe2cIMdTRlRyyW2u9/m+npse7KJ/02sLDhtKV6puuhqUo1fyVLuIkihAZxsPnLU6kb6KMn6PM06W+sKhhxlnNdGiTGUuhHZTEnhgTwvMQdzr0FgJiaZH6ngvk8l2CisbfgPGkCIr5EEZFvjQDe0uYicu+AgcQYcw6F+ZpEQ8hXpwESke0UIRTXRh2ido+z1gYhfkMcdIO30R+FAm2itstZL4URX9AL058FAy2iNsxZ84YR4V0RLgQeK+ElaiOc9y1gxNQ+7Xui0pPCU2gRy5z3nuDE7yBEiJBliU1ELHPeP0QQX9G8zSwsYOwR7z1geiJQWKMqVVDE4E+89/HpiSBhaMDY4B+8ZzHoiQAh5OiaEOET3vM09MSgMERgbGiE90wUhjgZ3AUHhTfFLta8Qu5zbRjiZG93N07YFabwEffZRAzRFHYjhde6whQO/oP7fCmGaAm9xqTPF67wCfcZYQyxLuwGVxMtX6jCoSn+c95oYlPoGJM+X6jCmICz+miiS9hQWsJrTV6oQmNFI+B5CxTRLzTDbQtd+LOQZ2ZQxDYLh0bEPPeEIJ6k2yqMiXp2DS4ca6vQnA3FPH8YVeHQE1HPkEZVOFgW9RxwRIXWXCHmWe6oCp8Iex4/osKhsrB3KsB8KrFQyE1DX9QbqZD3YoB9o9+vZkmFr/rFV0vtkVTMu01AkV19tvbskkw4+XAmMyO8GDUoe4WCJ311dKCzs3PgRZJA2PU6E4/HZwT3Rnu6dwlF16O6a4aws0aSwy7dPoMh9r7FkP8dQ7zVGlV1v2c8+9RMoRkKXlg/ZnLzrTuJ3IPPI9kv5NhCmbqx629GHeDztc5OCNEvdI5GZR46qP0XNya5lNbGyStkHGvszzkd993puzNqY7OqdN8BdnamkUL3WaiZ/v2Gat/8gNLxj6/2WZVD5aCQ6Z17Ruo67thfBOqTskYq7z9/qqwNdHoCIfQd95q5+fph/6TBitmlZOM3v/jG0j+b4wzvexPV6873cvqeP+0eWFsbqHUGQoEJg2f29ExmJhP/5aFTuuoDvjkCl0LQexNZ1jXOOz+MANkakQQJ47A3gOiZmy4hzSHhRgr/KYOEtBOGMb7cdr8wEe5zG69B8+cJV4X1pTnmUKYQ/P5SqnfQGrz7Tytr6Q5SoTHkKMmmEM3zCtMz+uv+fSrkTzJYSJ5ENTv29JnZKnsphHbE49DGCREqun4zM/OaYuU6BHuPMHESs7dfrtmdjklIEh6h+YObFEZ3Clne562ql88ag0rrhKbxYYzIOAR/nzfRcJod63bmu1YKjfmyiySNjY0hUEgwJ2ZP1lyzQmuFcZ1kB+JNIfW3EbKr7hVZq4UkRNdyBiTEnZBSTzzAlgsJ9pGD6O9bYL5Rot72Atsg1GfQ5/eHfpbRQvQ9DPXQtzJrvTCu/44UPvKDAkLUjOHe97VNGM+gSjpDgY+uUn3vadQPbIswPjMIBfqHGaAQvhXOXg74r7ctQncxADPMgIXw764FgO0R6hlYEhs1UowQtrJRvw800vYI4xnIOTH3thAphBzOyL4IbnHbI7z5GtJMQZ8ipfmGZbCRtkkYzwCFoDZK8x1SdSzYSNslnJkEtdE/gBbyb8mq96MjzACWboOBuR4pBKxP1b9HqJWC3qFM9z1gwJQRcSG4EyKEwQJxtIWQTogSBoo2kRb69vVkQv9oE2khZJTBCMt/HSFslMEI5RHtLyIMbpkIhd4BNbpC6DCKF8rTib+AcOhfSANa6CZGVRgozNAJ5blExIVD0ImQUOgQoynEAvHCJjGSQjyQQCjfS0RWSAAkEdaHmwgKcYMMsVCeT0RSiJkmaITyihZBIXqipxQaCzgtYsLBGGqpRi+Uy3ktWsJHiMU2k1CWd/4XIeF/fyIFUgjl//wZFaH+4N/kl00hlN/HAyXh9lT1H7+nuGoaoVz+ehQB4envFzQXTSWU5Y8z7Rbqj8/oLplSKF9c86Sx5cLMbzQtlEUoy5/caWz1aZMH76ivl14ov684aWyt8PSUNoFsQlk+a6axlUL9MX0CWYXyRfdRq4X6aRdDApmFsnweP2qp8PT0V8YrZRWaTZX1fCm9MMMwwvAL5Yt3f7ZI+OCYao4XJjSMHzpbIMx85fBxCk1jOmRh+gPbACNKaLbVdIjC9Aeu/AkRGsazdG8owt7OM26fEKERv3b0Chf2drDOD94QIzSWcme9R4hnZiiF+mnvGWf3a4YooRHv32X820cWoa6fPv56Lu6yBAqNHfL5h6MjjBLDy5w++HpOXIMhCaFCM96fKTOo9orW/fbuXMDg4gnhQiMuzt8ZSkgu4Tr9+FfROjPCEJpxcX52eGowA9kM9LrM6emDx2+N3Altm06EJbTi4vzjp69dJtSg1mqOUDefpDRoRrM8PP4ovGF6IlShFWX54v35x49nnz4fHnZbH7bo6vr97aezs4/n7y/kkBLniv8DIpSLcE8VQigAAAAASUVORK5CYII="
                            alt="icon"
                            style={{height:'180px'}}

                        />
                    <p style={{width: '50%', margin: 'auto', paddingTop: '1em' }}>
                    The isles of Greece, the isles of Greece!
                    Where burning Sappho loved and sung,
                    Where grew the arts of war and peace,
                    Where Delos rose, and Phoebus
                    sprung!
                    Eternal summer gilds them yet,
                    But all, except their sun, is set...
                    </p>

                    </Cell>
                    <Cell col={6}>
                        <h2>Contact Me</h2>
                        <hr/>

                        <div className="contact-list">               
                                <List>
                                    <ListItem>
                                        <ListItemContent style={{fontSize:'30px',fontFamily: 'Source Sans Pro' }}>
                                            <i className="fa fa-phone-square" aria-hidden="true"/>
                                            (+48) 794-551-151
                                            
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent style={{fontSize:'30px',fontFamily: 'Source Sans Pro' }}>
                                            <i className="fa fa-envelope" aria-hidden="true"/>
                                            arkadiuszkrezel88@gmail.com
                                            
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent style={{fontSize:'30px',fontFamily: 'Source Sans Pro' }}>
                                            <i className="fa fa-skype" aria-hidden="true"/>
                                            Sky
                                            
                                        </ListItemContent>
                                    </ListItem>
                                </List>
                        </div>
                    </Cell>
                </Grid>
            </div>

        )
    }
}

    export default Contact;