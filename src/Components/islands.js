import React, {Component} from 'react';
import { Grid,Cell} from 'react-mdl';
import './image/Balos.jpg';
import './image/Rhodos.jpg';
import './image/Zakyntos.jpeg';
import styled, { css } from "styled-components";
import {Link} from 'react-router-dom'


const Button = styled.button`
  border-radius: 10px;
  padding: 0.25em 1em;
  margin: 0 1em;
  background: transparent;
  color: palevioletred;
  border: 15px solid palevioletred;

  ${props => props.primary && css`
    background: palevioletred;
    color: white;
  `}
`;

class Islands extends Component {
    render (){
        return(
            <div style={{width: '100%', margin: 'auto', padding: '5px 0'}}>
                <Grid className= 'islands-grid'>
                    <Cell col={4}><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Chania_-_Lighthouse.jpg/240px-Chania_-_Lighthouse.jpg" alt="balos-img"/>
                        <Link to="https://en.wikipedia.org/wiki/Crete">
                            <h1><Button primary>Crete</Button></h1>
                        </Link>
                    </Cell>

                    <Cell col={4}><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Rhodes_Old_Town_2.JPG/240px-Rhodes_Old_Town_2.JPG" alt="rhodos-img"/>
                        <Link to='https://en.wikipedia.org/wiki/Rhode_Island'>
                            <h2><Button primary>Rhodes</Button></h2>
                        </Link>
                    </Cell>
                    
                    <Cell col={4}><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Zakintos_-_panorama.jpg/240px-Zakintos_-_panorama.jpg" alt="zakynthos-img"/>
                        <Link to='https://en.wikipedia.org/wiki/Zakynthos'>
                            <h3><Button primary>Zakynthos</Button></h3>
                         </Link>
                    </Cell>

                </Grid>

                

        </div>
        )
    }
}

    export default Islands;