import React, {Component} from 'react';
import { Grid,Cell} from 'react-mdl';


class Home extends Component {
    render (){
        return(
            <div style={{width: '100%', margin: 'auto', }}>
                <Grid className= 'home-grid'>
                    <Cell col={12}>
                        <img
                            src="https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Greece.svg"
                            alt="balos"
                            clasName="greece-img"
                            />
                            <div className="greece-text">
                                <h1> Greece's Adventures</h1>
                            
                            <hr/>
                            <p>| Crete | Rhodes | Corfu | Zakynthos | Lesbos | Kos | </p> 
                            </div>
            
                    </Cell>
                </Grid>

            </div>

        )
    }
}

    export default Home;