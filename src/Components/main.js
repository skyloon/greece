import React from 'react';
import Home from "./home";
import {Switch, Route} from 'react-router-dom';
import Aboutme from './aboutme';
import Contact from './contact';
import Islands from './islands';
const Main = () => (
    <Switch>
        <Route exact path="/home" component={Home} />
        <Route  path="/aboutme" component={Aboutme} />
        <Route  path="/contact" component={Contact} />
        <Route  path="/islands" component={Islands} />

        </Switch>
)

export default Main;