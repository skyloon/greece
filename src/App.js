import React, { Component } from 'react';
import './App.css';
import {Layout,Header,Navigation,Drawer,Content,Footer,FooterSection,FooterLinkList} from 'react-mdl';
import Main from './Components/main';
import {Link} from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <div className="demo-big-content">
    <Layout>
        <Header className="header-color" title="Traveling around Greece" scroll>
            <Navigation>
                <Link to="/home">Home</Link>
                <Link to="/aboutme">About me</Link>
                <Link to="/islands">Islands</Link>
                <Link to="/contact">Contacts</Link>
            </Navigation>
        </Header>
        <Drawer title="Islands">
            <Navigation>
            <Link to="/home">Home</Link>
                <Link to="/aboutme">About me</Link>
                <Link to="/islands">Islands</Link>
                <Link to="/contact">Contacts</Link>
            </Navigation>
        </Drawer>
        <Content>
            <div className="page-content" />
            <Main/>
        </Content>
    </Layout>
    <div class="footer">
    <Footer size="mini">
    <FooterSection type="left" logo="Greece's Traditional stuff:">
        <FooterLinkList>
            <a href="https://greece.greekreporter.com/2013/10/09/10-1-most-popular-greek-products-2/" rel="products" target="_blank">Products</a>
            <a href="https://www.greeka.com/greece/greece-shopping.htm" rel="shops" target="_blank">Shops</a>
           
        </FooterLinkList>
    </FooterSection>
</Footer>
</div>
    

</div>
    );
  }
}


export default App;
